package com.auth.authsec.ui.user;

import java.util.List;

import javax.validation.constraints.Size;

/**
 * Class for representing the User model in a browser.
 * 
 * @author Plamen
 *
 */
public class UserForm {

	/**
	 * The username. Should not be less than 5 characters.
	 */
	@Size(min = 5, message = "Username is too short!")
	private String username;

	/**
	 * The password. Should not be less than 5 characters.
	 */
	@Size(min = 5, message = "Password is too short!")
	private String password;

	private List<String> roles;
	
	/**
	 * Default Constructor
	 */
	public UserForm() {
		super();
	}

	public UserForm(List<String> roles) {
		this.roles = roles;
	}
	
	public UserForm(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public UserForm(String username, String password, List<String> roles) {
		this(username, password);
		this.roles = roles;
	}
	
	/**
	 * Getter for the username
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Setter for the username
	 * 
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Getter for the password
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter for the password
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the roleName
	 */
	public List<String> getRoles() {
		return roles;
	}

	/**
	 * @param roleName the roleName to set
	 */
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
